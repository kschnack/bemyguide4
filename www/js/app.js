// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('starter', ['ionic', 'starter.controllers', 'starter.services'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

      // Don't remove this line unless you know what you are doing. It stops the viewport
      // from snapping when text inputs are focused. Ionic handles this internally for
      // a much nicer keyboard experience.
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider){
  $stateProvider

  .state('front', {
    url: '/front',
    templateUrl: 'templates/frontPage.html'
  })

  .state('register', {
    url: '/register',
    templateUrl: 'templates/registerPage.html'
  })

  .state('login', {
    url: '/login',
    templateUrl: 'templates/loginPage.html',
    controller: 'LoginCtrl'
  })

  // setup an abstract state for the tabs directive
  .state('tab', {
    url: '/tab',
    abstract: true,
    templateUrl: 'templates/tabs.html'
  })

  .state('tab.matches', {
    url: '/matches',
    views: {
      'tab-matches': {
              templateUrl: 'templates/tab-matches.html',
              controller: 'MatchesCtrl'
          }
      }
  })

  .state('tab.find-match', {
    url: '/find-match',
    views: {
      'tab-find-match': {
        templateUrl: 'templates/tab-find-match.html',
        controller: 'FindMatchCtrl'
      }
    }
  })

  .state('tab.settings', {
    url: '/settings',
    views: {
        'tab-settings': {
          templateUrl: 'templates/tab-settings.html',
          controller: 'SettingsCtrl'
      }
    }
  })

  .state('tab.about', {
    url: '/about',
    views: {
      'tab-about': {
        templateUrl: 'templates/tab-about.html',
        controller: 'AboutCtrl'
      }
    }
  })

  $urlRouterProvider.otherwise('/front');
});
