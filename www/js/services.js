angular.module('starter.services', [])

.factory('userService', function($http, $q){
  return{
	  getUsers: function(){
	 	var deferred = $q.defer();

	  	$http.get('http://bemyguidewebapi.azurewebsites.net/api/user').success(function(data){
		  		deferred.resolve(data);
		  	}).error(function(){
				console.log("Error while making HTTP call.");
				deferred.reject();
			})

	  	return deferred.promise;
	  }
  }

})

.service('LoginService', function($q){
	return {
		loginUser: function(name, pw){
			var deferred = $q.defer();
			var promise = deferred.promise;

			if (name == 'user' && pw == 'secret'){
				deferred.resolve('Welcome' + name + '!');
			} else {
				deferred.reject('Wrong credentials.');
			}
			promise.success = function(fn) {
				promise.then(fn);
				return promise;
			}
			promise.error = function(fn) {
				promise.then(null, fn);
				return promise;
			}
			return promise;
		}
	}
});