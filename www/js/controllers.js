angular.module('starter.controllers', [])

.controller('LoginCtrl', function($scope, LoginService, $ionicPopup, $state){
	$scope.data = {};

	$scope.login = function(){
		console.log('Login user: ' + $scope.data.username + ' - PW' + $scope.data.password)
		LoginService.loginUser($scope.data.username, $scope.data.password).success(function(data){
			$state.go('tab.matches');
		}).error(function(data){
			var alertPopup =$ionicPopup.alert({
				title: 'Login failed!',
				template: 'Please check your credentials.'
			});
		});
	}
})

.controller('MatchesCtrl', function($scope, userService){
  $scope.users = []; 

  userService.getUsers().then(function(data){
    $scope.users = data;
  })
})

.controller('FindMatchCtrl', function($scope, userService){
	$scope.users = [];

	userService.getUsers().then(function(data){
		$scope.users = data;
	})
})

.controller('SettingsCtrl', function($scope) {})

.controller('AboutCtrl', function($scope) {});
